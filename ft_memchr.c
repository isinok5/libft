/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memchr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: wsalmon <wsalmon@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/09/13 19:00:33 by wsalmon           #+#    #+#             */
/*   Updated: 2019/09/13 19:00:39 by wsalmon          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	*ft_memchr(const void *arr, int c, size_t n)
{
	const unsigned char *ar;

	ar = (unsigned char *)arr;
	while (n > 0)
	{
		if (*ar == (unsigned char)c)
			return ((void *)ar);
		ar++;
		n--;
	}
	return (0);
}
