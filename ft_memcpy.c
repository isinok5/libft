/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memcpy.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: wsalmon <wsalmon@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/09/13 16:48:29 by wsalmon           #+#    #+#             */
/*   Updated: 2019/09/13 16:48:30 by wsalmon          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	*ft_memcpy(void *dest, const void *src, size_t n)
{
	char		*buf;
	char		*result;

	buf = (char *)src;
	result = (char *)dest;
	if (buf == result || n == 0)
		return (dest);
	n--;
	while (n)
	{
		*result = *buf;
		result++;
		buf++;
		n--;
	}
	*result = *buf;
	return (dest);
}
