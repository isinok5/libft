/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memccpy.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: wsalmon <wsalmon@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/09/13 20:25:17 by wsalmon           #+#    #+#             */
/*   Updated: 2019/09/13 20:25:18 by wsalmon          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	*ft_memccpy(void *dest, const void *source, int c, size_t n)
{
	unsigned char	*result;
	unsigned char	*src;
	size_t			i;

	i = 0;
	result = (unsigned char *)dest;
	src = (unsigned char *)source;
	if (result == src || n == 0)
		return (NULL);
	while (i < n)
	{
		result[i] = src[i];
		if (result[i] == (unsigned char)c)
			return (dest + i + 1);
		i++;
	}
	return (NULL);
}
