/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strrev.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: wsalmon <wsalmon@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/09/22 17:19:00 by wsalmon           #+#    #+#             */
/*   Updated: 2019/09/22 17:19:02 by wsalmon          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strrev(char *str)
{
	unsigned int	i;
	unsigned int	j;
	char			buf;

	i = 0;
	j = ft_strlen(str) - 1;
	while (j > i)
	{
		buf = str[i];
		str[i] = str[j];
		str[j] = buf;
		i++;
		j--;
	}
	return (str);
}
