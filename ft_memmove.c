/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memmove.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: admin <admin@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/09/13 17:28:54 by wsalmon           #+#    #+#             */
/*   Updated: 2019/09/17 15:11:47 by admin            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	*ft_memmove(void *dest, const void *source, size_t n)
{
	unsigned char		*result;
	const unsigned char	*src;
	size_t				i;

	if (dest == source || n == 0)
		return (dest);
	result = (unsigned char *)dest;
	src = (unsigned char *)source;
	i = 0;
	if (src < result)
	{
		while (++i <= n)
		{
			result[n - i] = src[n - i];
		}
	}
	else
	{
		while (n > 0)
		{
			*result++ = *src++;
			n--;
		}
	}
	return (dest);
}
