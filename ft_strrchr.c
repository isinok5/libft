/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strrchr.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: wsalmon <wsalmon@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/09/11 18:41:59 by wsalmon           #+#    #+#             */
/*   Updated: 2019/09/11 19:45:26 by wsalmon          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strrchr(const char *str, int ch)
{
	char *ptr;

	ptr = (char *)str + ft_strlen(str);
	while (*ptr != ch)
	{
		if (ptr == str)
			return (NULL);
		ptr--;
	}
	return (ptr);
}
